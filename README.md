# Announcing events web application

### This application allows something(create, edit, read)

To run this application locally you must follow the steps indicated below

1. Close this repository

bash
git clone https://gitlab.com/sulaymonss/firdavs.git

2. Install dependencies

bash
cd events
yarn install

3. Run the application

bash
yarn dev

### web application dependencies list

- node js
- express.js
- fs, json

### Web application repository on gitlab

https://gitlab.com/sulaymonss/firdavs